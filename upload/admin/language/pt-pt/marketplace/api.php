<?php
// Heading
$_['heading_title']    = 'Norte Marketplace API';

// Text
$_['text_success']     = 'Success: You have modified your API information!';
$_['text_signup']      = 'Please enter your Norte API information which you can obtain <a href="https://www.leetio.dev/index.php?route=account/store" target="_blank" class="alert-link">here</a>.';

// Entry
$_['entry_username']   = 'Username';
$_['entry_secret']     = 'Secret';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify marketplace API!';
$_['error_username']   = 'Username required!';
$_['error_secret']     = 'Secret required!';
