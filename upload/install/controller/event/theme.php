<?php
class ControllerEventTheme extends Controller {
	public function index(&$view, &$data) {
		// Twig needs Autoloader
		if ($this->config->has('vendor_autoload') 
		&& $this->config->get('vendor_autoload')) {

			if (is_file(DIR_TEMPLATE . $view . '.twig')) {
				$this->config->set('template_engine', 'twig');
				return;
			}
		}	

		if (is_file(DIR_TEMPLATE . $view . '.tpl')) {
			$this->config->set('template_engine', 'template');
			return;
		}		
	}
}
