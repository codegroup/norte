<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:9000/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:9000/');

// DIR
define('DIR_APPLICATION', '/home/s1b9/norte/upload/catalog/');
define('DIR_SYSTEM', '/home/s1b9/norte/upload/system/');
define('DIR_IMAGE', '/home/s1b9/norte/upload/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'pdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'ec-user');
define('DB_PASSWORD', '3c-us3r');
define('DB_DATABASE', 'ec');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');