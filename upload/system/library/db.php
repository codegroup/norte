<?php
/**
 * @package		Norte
 * @author		CodeGroup Team
 * @copyright	Copyright (c) 2021, Norte, Ltd. (https://wiki.cyberporto.xyz/Software/Norte
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://wiki.cyberporto.xyz/Software/Norte
 */

/**
 * DB class
 */
class DB {
    private $adaptor;

    /**
     * Constructor
     *
     * @param	string	$adaptor
     * @param	string	$hostname
     * @param	string	$username
     * @param	string	$password
     * @param	string	$database
     * @param	int		$port
     *
     */
    public function __construct($adaptor, $hostname, $username, $password, $database, $port = NULL) {
        $class = 'DB\\' . $adaptor;

        if (class_exists($class)) {
            $this->adaptor = new $class($hostname, $username, $password, $database, $port);
        } else {
            throw new \Exception('Error: Could not load database adaptor ' . $adaptor . '!');
        }
    }

    /**
     *
     *
     * @param	string	$sql
     *
     * @return	array
     */
    public function query($sql) {
        return $this->adaptor->query($sql);
    }

    public function prepare($sql) {
        return $this->adaptor->prepare($sql);
    }
    /**
     *
     *
     * @param	string	$value
     *
     * @return	string
     */
    public function escape($value) {
        return $this->adaptor->escape($value);
    }

    /**
     *
     *
     * @return	int
     */
    public function countAffected() {
        return $this->adaptor->countAffected();
    }

    /**
     *
     *
     * @return	int
     */
    public function getLastId() {
        return $this->adaptor->getLastId();
    }

    /**
     *
     *
     * @return	bool
     */
    public function connected() {
        return $this->adaptor->connected();
    }
}
