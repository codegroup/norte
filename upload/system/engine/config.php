<?php
/**
 * @package		Norte
 * @author		CodeGroup Team
 * @copyright	Copyright (c) 2021, Norte, Ltd. (https://wiki.cyberporto.xyz/Software/Norte
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://wiki.cyberporto.xyz/Software/Norte
 */

/**
 * Config class
 */
class Config {
    private $data = array();
    private $dir = NULL;

    public function set_dir($dir) {
        // root path for configuration files
        $this->dir = $dir;
    }


    /**
     *
     *
     * @param	string	$key
     *
     * @return	mixed
     */
    public function get($key) {
        return (isset($this->data[$key]) ? $this->data[$key] : null);
    }

    /**
     *
     *
     * @param	string	$key
     * @param	string	$value
     */
    public function set($key, $value) {
        $this->data[$key] = $value;
    }

    /**
     *
     *
     * @param	string	$key
     *
     * @return	mixed
     */
    public function has($key) {
        return isset($this->data[$key]);
    }

    /**
     *
     *
     * @param	string	$filename
     */
    public function load($filename) {
        $file = $this->dir . $filename . '.php';

        if (file_exists($file)) {
            $_ = array();

            require($file);

            $this->data = array_merge($this->data, $_);
        } else {
            trigger_error('Error: Could not load config ' . $filename . '!');
            exit();
        }
    }
}
