<?php
/**
 * @package		Norte
 * @author		CodeGroup Team
 * @copyright	Copyright (c) 2021, Norte, Ltd. (https://wiki.cyberporto.xyz/Software/Norte
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://wiki.cyberporto.xyz/Software/Norte
*/

/**
* Controller class
*/
abstract class Controller {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}