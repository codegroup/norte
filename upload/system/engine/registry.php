<?php
/**
 * @package		Norte
 * @author		CodeGroup Team
 * @copyright	Copyright (c) 2021, Norte, Ltd. (https://wiki.cyberporto.xyz/Software/Norte
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://wiki.cyberporto.xyz/Software/Norte
*/

/**
* Registry class
*/
final class Registry {
	private $data = array();

	/**
     * 
     *
     * @param	string	$key
	 * 
	 * @return	mixed
     */
	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : null);
	}

    /**
     * 
     *
     * @param	string	$key
	 * @param	string	$value
     */	
	public function set($key, $value) {
		$this->data[$key] = $value;
	}
	
    /**
     * 
     *
     * @param	string	$key
	 *
	 * @return	bool
     */
	public function has($key) {
		return isset($this->data[$key]);
	}
}